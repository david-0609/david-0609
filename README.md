### **Hello! I am a hobbyist coder with a main focus on backend development.** <br /> **I am also an advocate for Free and Open Source Software and Linux user since 2018/19.**

#### Skills/Experience
- Python
- Linux command line
- Working with 
    - APIs
    - ORM Databases
- Rust
- Neovim

#### What I am  learning:
- Zig

#### Current Projects:
- [remember-cmd](https://gitlab.com/david-0609/remember-cmd)

#### Future Projects:

#### Past Projects:
- OpenCV Hand Gesture Control
- [nvim-restremind](https://gitlab.com/david-0609/nvim-restremind)
- Custom Neovim config
- discordpy Ouroborosbot
- some html/css websites
- [matrix-modbot](https://gitlab.com/david-0609/matrix-modbot)
- [template-portfolio-tailwindcss](https://david-0609.gitlab.io/template_portfolio_tailwindcss/)

#### [My profile on Github](https://github.com/david-0609/)
